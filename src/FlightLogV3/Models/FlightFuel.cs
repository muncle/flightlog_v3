﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightLogV3.Models
{
    public class FlightFuel
    {
        public int FlightFuelID { get; set; }
        public Nullable<int> FuelUplift { get; set; }
        public Nullable<int> FuelUnits { get; set; }
        public Nullable<int> FuelProvider { get; set; }
        public string FuelReceiptNumber { get; set; }
        public int FlightLogFlightLogID { get; set; }

        public virtual FlightLog FlightLog { get; set; }
    }
}
