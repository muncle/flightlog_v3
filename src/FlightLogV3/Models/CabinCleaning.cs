﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightLogV3.Models
{
    public class CabinCleaning
    {
        public int CabinCleaningID { get; set; }
        public string CabinCleaningTypes { get; set; }

        public virtual ICollection<FlightCleaning> FlightCleaning { get; set; }
    }
}
