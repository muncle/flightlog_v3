﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightLogV3.Models
{
    public class Region
    {
        public int RegionID { get; set; }
        public string RegionName { get; set; }
    }
}
