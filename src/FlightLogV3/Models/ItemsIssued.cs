﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightLogV3.Models
{
    public class ItemsIssued
    {
        public int ItemIssuedID { get; set; }
        public string PartNumber { get; set; }
        public string SerialNumber { get; set; }
        public string GRNBatchNumber { get; set; }
        public Nullable<int> QuantityIssued { get; set; }
    }
}
