﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightLogV3.Models
{
    public class ArrivingStaff
    {
        public int ArrivingStaffID { get; set; }
        public int FlightLogFlightLogID { get; set; }
        public int StaffStaffID { get; set; }

        public virtual FlightLog FlightLog { get; set; }
        public virtual Staff Staff { get; set; }
    }
}
