﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightLogV3.Models
{
    public class SparesTooling
    {
        public int PartNumberID { get; set; }
        public string PartNumber { get; set; }
        public string Description { get; set; }
        public Nullable<decimal> SalePrice { get; set; }
        public Nullable<int> SalePriceCurrency { get; set; }

        public virtual ICollection<SparesDetail> SparesDetail { get; set; }
    }
}
