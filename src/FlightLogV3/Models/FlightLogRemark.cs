﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightLogV3.Models
{
    public class FlightLogRemark
    {
        public int FlightLogRemarksID { get; set; }
        public string FlightLogRemarks { get; set; }
        public int FlightLogFlightLogID { get; set; }

        public virtual FlightLog FlightLog { get; set; }
    }
}
