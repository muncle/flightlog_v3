﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightLogV3.Models
{
    public class UnitsOfMeasure
    {
        public int UnitsOfMeasureID { get; set; }
        public string UnitsOfMeasureCode { get; set; }
        public string UnitsOfMeasureDescription { get; set; }
    }
}
