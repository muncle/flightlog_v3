﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightLogV3.Models
{
    public class FlightLog
    {
        public int FlightLogID { get; set; }
        public System.DateTime ArrivalDate { get; set; }
        public string ArrivalFlightNumber { get; set; }
        public System.TimeSpan ActualArrivalTime { get; set; }
        public string ArrivalParkingBay { get; set; }
        public System.DateTime DepartureDate { get; set; }
        public string DepartureParkingBay { get; set; }
        public string DepartureFlightNumber { get; set; }
        public System.TimeSpan ActualDepartureTime { get; set; }
        public string FlightRemarks { get; set; }
        public Nullable<System.DateTime> RecordCreated { get; set; }
        public int AirportAirportID { get; set; }
        public string AircraftAircraftSerialNumber { get; set; }
        public int AirlineAirlineID { get; set; }
        public Nullable<int> FlightLogInputUser { get; set; }

        public virtual ICollection<ArrivingStaff> ArrivingStaffs { get; set; }
        public virtual ICollection<DepartingStaff> DepartingStaffs { get; set; }
        public virtual ICollection<FSNIn> FSNIn { get; set; }
        public virtual ICollection<FSNOut> FSNOut { get; set; }
        public virtual ICollection<FlightCleaning> FlightCleaning { get; set; }
        public virtual ICollection<FlightFuel> FlightFuel { get; set; }
        public virtual ICollection<FlightGSE> FlightGSE { get; set; }
        public virtual ICollection<FlightLogRemark> FlightLogRemark { get; set; }
        public virtual ICollection<FlightVendorService> FlightVendorServices { get; set; }
        public virtual Airport Airport { get; set; }
        public virtual Aircraft Aircraft { get; set; }
        public virtual Airline Airline { get; set; }
        public virtual ICollection<Delay> Delay { get; set; }
        public virtual ICollection<LoanSpare> LoanSpare { get; set; }
        public virtual ICollection<Delay> Delays { get; set; }
    }
}
