﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightLogV3.Models
{
    public class FSNIn
    {
        public int FSNInID { get; set; }
        public string FSNInNumber { get; set; }
        public int FlightLogFlightLogID { get; set; }

        public virtual FlightLog FlightLog { get; set; }
    }
}
