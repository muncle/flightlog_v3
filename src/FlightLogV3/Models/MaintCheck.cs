﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightLogV3.Models
{
    public class MaintCheck
    {
        public int MaintCheckID { get; set; }
        public string MaintCheckCode { get; set; }
        public string MaintCheckDescription { get; set; }

        public virtual ICollection<AirlineMaintCheck> AirlineMaintCheck { get; set; }
    }
}
