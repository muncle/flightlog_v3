﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightLogV3.Models
{
    public class VendorGroundEquipment
    {
        public int VendorGSEID { get; set; }
        public Nullable<decimal> ContractRate { get; set; }
        public Nullable<int> Currency { get; set; }
        public Nullable<int> UnitOfMeasure { get; set; }
        public Nullable<int> Tax { get; set; }
        public int VendorVendorsID { get; set; }
        public int GroundEquipmentGroundEquipmentID { get; set; }

        public virtual Vendor Vendor { get; set; }
        public virtual GroundEquipment GroundEquipment { get; set; }
    }
}
