﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightLogV3.Models
{
    public class Aircraft
    {
        public string AircraftSerialNumber { get; set; }
        public string AircraftRegistration { get; set; }
        public int AircraftTypesID { get; set; }
        public int AirlineID { get; set; }

        public virtual AircraftType AircraftType { get; set; }
        public virtual Airline Airline { get; set; }
        public virtual ICollection<FlightLog> FlightLog { get; set; }
    }
}
