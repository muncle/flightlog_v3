﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightLogV3.Models
{
    public class FlightVendorService
    {
        public int fvtFlightVendorID { get; set; }
        public Nullable<double> UnitsUsed { get; set; }
        public Nullable<int> Units { get; set; }
        public int FlightLogFlightLogID { get; set; }
        public int ServiceServicesID { get; set; }
        public int VendorVendorsID { get; set; }

        public virtual FlightLog FlightLog { get; set; }
        public virtual Service Service { get; set; }
        public virtual Vendor Vendor { get; set; }
    }
}
