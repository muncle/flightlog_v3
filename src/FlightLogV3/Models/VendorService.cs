﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightLogV3.Models
{
    public class VendorService
    {
        public int VendorServicesID { get; set; }
        public Nullable<decimal> ContractRate { get; set; }
        public Nullable<int> Currency { get; set; }
        public string UnitOfMeasure { get; set; }
        public Nullable<int> Tax { get; set; }
        public int ServiceServicesID { get; set; }
        public int VendorVendorsID { get; set; }

        public virtual Service Service { get; set; }
        public virtual Vendor Vendor { get; set; }
    }
}
