﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightLogV3.Models
{
    public class Vendor
    {
        public int VendorsID { get; set; }
        public string VendorName { get; set; }

        public virtual ICollection<VendorService> VendorService { get; set; }
        public virtual ICollection<VendorGroundEquipment> VendorGroundEquipment { get; set; }
        public virtual ICollection<FlightVendorService> FlightVendorService { get; set; }
        public virtual ICollection<FlightGSE> FlightGSE { get; set; }
    }
}
