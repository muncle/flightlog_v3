﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightLogV3.Models
{
    public class JobTitle
    {
        public int JobTitleID { get; set; }
        public string JobTitleName { get; set; }
    }
}
