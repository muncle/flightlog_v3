﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightLogV3.Models
{
    public class AirlineMaintCheck
    {
        public int AirlineMaintenanceCheckID { get; set; }
        public int MaintenanceCheck { get; set; }
        public int AirlineID { get; set; }
        public int MaintCheckMaintCheckID { get; set; }

        public virtual Airline Airline { get; set; }
        public virtual MaintCheck MaintCheck { get; set; }
    }
}
