﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightLogV3.Models
{
    public class GroundEquipment
    {
        public int GroundEquipmentID { get; set; }
        public string GroundEquipmentDescription { get; set; }

        public virtual ICollection<VendorGroundEquipment> VendorGroundEquipment { get; set; }
        public virtual ICollection<FlightGSE> FlightGSE { get; set; }
    }
}
