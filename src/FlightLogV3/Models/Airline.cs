﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightLogV3.Models
{
    public class Airline
    {
        public int AirlineID { get; set; }
        public string AirlineName { get; set; }
        public string AirlineICAOCode { get; set; }
        public string AirlineIATACode { get; set; }
        public string AirlineLogoSmall { get; set; }
        public string AirlineLogoLarge { get; set; }

        public virtual ICollection<Aircraft> Aircraft { get; set; }
        public virtual ICollection<AirlineMaintCheck> AirlineMaintChecks { get; set; }
        public virtual ICollection<FlightLog> FlightLog { get; set; }
    }
}
