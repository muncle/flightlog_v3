﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightLogV3.Models
{
    public class ContactType
    {
        public int ContactTypeID { get; set; }
        public string ContactTypeName { get; set; }
    }
}
