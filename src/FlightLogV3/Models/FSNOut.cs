﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightLogV3.Models
{
    public class FSNOut
    {
        public int FSNOutID { get; set; }
        public string FSNOutNumber { get; set; }
        public int FlightLogFlightLogID { get; set; }

        public virtual FlightLog FlightLog { get; set; }
    }
}
