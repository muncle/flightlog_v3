﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightLogV3.Models
{
    public class Country
    {
        public int CountryID { get; set; }
        public string CountryName { get; set; }
        public string CountryCodeShort { get; set; }
        public string CountryCodeLong { get; set; }
        public string CountryDialCode { get; set; }
    }
}
