﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightLogV3.Models
{
    public class DelayDetail
    {
        public int DelayDetailsID { get; set; }
        public string DelayCode { get; set; }
        public Nullable<int> DelayMinutes { get; set; }
        public int DelayDelayID { get; set; }

        public virtual Delay Delay { get; set; }
    }
}
