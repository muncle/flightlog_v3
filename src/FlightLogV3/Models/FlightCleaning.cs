﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightLogV3.Models
{
    public class FlightCleaning
    {
        public int FlightCleaningID { get; set; }
        public int FlightLogFlightLogID { get; set; }
        public int CabinCleaningCabinCleaningID { get; set; }

        public virtual FlightLog FlightLog { get; set; }
        public virtual CabinCleaning CabinCleaning { get; set; }
    }
}
