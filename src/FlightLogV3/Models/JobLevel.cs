﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightLogV3.Models
{
    public class JobLevel
    {
        public int JobLevelID { get; set; }
        public string JobLevelDescription { get; set; }
    }
}
