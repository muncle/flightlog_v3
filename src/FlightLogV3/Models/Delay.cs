﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightLogV3.Models
{
    public class Delay
    {
        public int DelayID { get; set; }
        public Nullable<bool> EngineeringDelay { get; set; }
        public string EngineeringDelayDescription { get; set; }
        public int DelayFlightLogID { get; set; }

        public virtual ICollection<DelayDetail> DelayDetail { get; set; }
        public virtual FlightLog FlightLog { get; set; }
    }
}
