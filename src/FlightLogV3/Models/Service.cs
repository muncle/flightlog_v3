﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightLogV3.Models
{
    public class Service
    {
        public int ServicesID { get; set; }
        public string Services { get; set; }

        public virtual ICollection<VendorService> VendorService { get; set; }
        public virtual ICollection<FlightVendorService> FlightVendorService { get; set; }
    }
}
