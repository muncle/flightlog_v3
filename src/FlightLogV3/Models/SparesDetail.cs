﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightLogV3.Models
{
    public class SparesDetail
    {
        public int PartNumberID { get; set; }
        public Nullable<int> OwnerAirline { get; set; }
        public string SerialNumber { get; set; }
        public string GRNBatchNumber { get; set; }
        public string Location { get; set; }
        public Nullable<System.DateTime> ExpiryDate { get; set; }
        public int SparesToolingPartNumberID { get; set; }
        public int SparesToolingPartNumberID1 { get; set; }

        public virtual SparesTooling SparesTooling { get; set; }
    }
}
