﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightLogV3.Models
{
    public class LoanSpare
    {
        public int LoanSparesID { get; set; }
        public Nullable<int> SpareTransaction { get; set; }
        public string Provider { get; set; }
        public string PartNumber { get; set; }
        public string SerialNumber { get; set; }
        public string GRN_BatchNumber { get; set; }
        public string TransactionNumber { get; set; }
        public int FlightLogFlightLogID { get; set; }

        public virtual FlightLog FlightLog { get; set; }
    }
}
