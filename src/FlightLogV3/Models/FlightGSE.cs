﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightLogV3.Models
{
    public class FlightGSE
    {
        public int FlightGSEID { get; set; }
        public Nullable<decimal> UnitsUsed { get; set; }
        public Nullable<int> Unit { get; set; }
        public int FlightLogFlightLogID { get; set; }
        public int VendorVendorsID { get; set; }
        public int GroundEquipmentGroundEquipmentID { get; set; }

        public virtual FlightLog FlightLog { get; set; }
        public virtual Vendor Vendor { get; set; }
        public virtual GroundEquipment GroundEquipment { get; set; }
    }
}
