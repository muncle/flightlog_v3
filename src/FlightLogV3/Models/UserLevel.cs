﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightLogV3.Models
{
    public class UserLevel
    {
        public int UserLevelID { get; set; }
        public string UserLevelDescription { get; set; }

        public virtual ICollection<Staff> Staffs { get; set; }
    }
}
