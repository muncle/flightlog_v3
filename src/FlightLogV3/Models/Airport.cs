﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightLogV3.Models
{
    public class Airport
    {
        public int AirportID { get; set; }
        public string AirportIATACode { get; set; }
        public string AirportName { get; set; }
        public string AirportCity { get; set; }
        public string AirportICAOCode { get; set; }
        public double AirportLatitude { get; set; }
        public double AirportLongitude { get; set; }
        public Nullable<System.DateTimeOffset> AirportTimeZone { get; set; }

        public virtual ICollection<FlightLog> FlightLog { get; set; }
    }
}
