﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightLogV3.Models
{
    public class Staff
    {
        public int StaffID { get; set; }
        public string StaffFirstName { get; set; }
        public string StaffLastName { get; set; }
        public string StaffPreferredName { get; set; }
        public string StaffEmail { get; set; }
        public Nullable<int> StaffPort { get; set; }
        public Nullable<bool> NotActive { get; set; }
        public byte[] StaffCreated { get; set; }
        public int StaffUserLevelID { get; set; }
        public string StaffPhoto { get; set; }
        public Nullable<System.DateTime> StaffDateOfJoin { get; set; }
        public Nullable<System.DateTime> StaffBirthday { get; set; }

        public virtual ICollection<ArrivingStaff> ArrivingStaff { get; set; }
        public virtual ICollection<DepartingStaff> DepartingStaff { get; set; }
        public virtual UserLevel UserLevel { get; set; }
    }
}
