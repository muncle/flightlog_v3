﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightLogV3.Models
{
    public class AircraftType
    {
        public int AircraftTypesID { get; set; }
        public string AircraftTypeRating { get; set; }
        public string AircraftICAOCode { get; set; }
        public string AircraftIATACode { get; set; }

        public virtual ICollection<Aircraft> Aircraft { get; set; }
    }
}
